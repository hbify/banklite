const models = require('../models');

module.exports = {

    index(req, res, next){
        const AccountTypes = models.AccountTypes;
        AccountTypes.findAll({include: [models.Account]}).then(accounts => {
                res.send(accounts);
            });
    }
}