const models = require('../models');

module.exports = {

    index(req, res, next){
        const Account = models.Account;
        Account.findAll({include: [ models.Transaction ]}).then(accounts => {
            res.send(accounts);
        });
    },

    showSystem(req, res, next){
        const Account = models.Account;
        Account.findAll({where: {accountType: "system"}}).then(accounts => {
            res.send(accounts);
        });
    }

}