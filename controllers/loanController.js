const models = require('../models');
const LoanInstallment = require('../services/loanCalculator');
const Customer = models.Customer;
const Loan = models.Loan;


module.exports = {

    index(req, res){
        Customer.findAll({include: [ {
            model: models.Account,
            include: [{
                model: models.Loan, 
                include: [models.Installment]
            }]} ]}).then(customers => {
            res.send(customers);
        });
    },
    create(req, res, next){
        const loan = req.body
        Loan.create(loan).then(loan => {
            res.send(loan);
        })
    },
    approve(req, res){
        const lId = req.body.loanId
        Loan.findById(lId).then(loan =>{
            loan.update({
                loanStatus: 0
              }).then((loan) => {
                  res.send(loan)
              })
        })
    },
    generate(req, res){
        const lId = req.body.loanId
        Loan.findById(lId).then(loan =>{
            const installments = LoanInstallment(loan.amount, loan.installmentsNumber,loan.interestRate);
            const Installment = models.Installment;
            const imap = installments.installments.map((instalment, index) => {
                const ins = {
                    installmentId: index+1,
                    capital: instalment.capital,
                    interest: instalment.interest,
                    remain: instalment.remain,
                    interestSum: instalment.interestSum,
                    installment: instalment.installment,
                    installmentType: 1,
                    LoanId: loan.id,
                    status: 1
                }
                return ins
            });
            Installment.bulkCreate(imap).then((instalments) => {
                res.send(instalments);
            })

            
        })
    },
    update(req, res){
        res.send('NOT IMPLEMENTED: update loan ');
    }
}