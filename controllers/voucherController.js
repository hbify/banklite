const models = require('../models');
const Voucher = models.Voucher;


module.exports = {
    index(req, res){
        Voucher.findAll({include: [models.Transaction]}).then(vouchers => {           
            res.send(vouchers);
        });
    },

    show(req, res){
        Voucher.findAll({where: {AccountId: req.body.AccountId}}).then(vouchers => {
            
            res.send(vouchers);
        });
    },

    filter(req, res){
        val = req.body;
        Voucher.findAll({where: val}).then(vouchers => {
            res.send(vouchers);
        });
    },
    
    create(req, res, next){
    
        Voucher.create(req.body).then((voucher)=>{
            res.send(voucher);
        });
    },

    update(req, res, next) {
        const voucherId = req.body.id;
        const voucherUpdate = req.body.voucher;
      
        Voucher.findById(voucherId).then(voucher => {
          voucher.update(voucherUpdate).then((v) => {
            if (v.confirmedBy){
                let account = models.Account;
                account.findById(v.AccountId).then(acc => {
                    if(v.vType === 0){
                        acc.balance += parseInt(v.amount);
                    }
                    else{
                        acc.balance -= parseInt(v.amount);
                    } 
                    acc.save().then(() => {})
                }); 
                
                let Transaction = models.Transaction;
                Transaction.findOne({where: {VoucherId: v.id}}).then((transaction)=>{
                        account.findById(transaction.AccountId).then(acc => {
                            if(transaction.transactionType == "deposit"){
                                acc.balance += transaction.amount;
                            }
                            else{
                                acc.balance -= transaction.amount;
                            } 
                            acc.save().then(() => {})
                        });
                })
        
            }
            res.send(v);
          })
        });
      }
}