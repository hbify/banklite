const jwt = require('jwt-simple');
const models = require('../models');
const config = require('../config');
const bcrypt = require('bcrypt-nodejs');
const User = models.User;

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, role: user.role, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
  // User has already had their email and password auth'd
  // We just need to give them a token
  res.send({ token: tokenForUser(req.user) });
}

exports.signup = function(req, res, next) {
  const email = req.body.email;
  const plainPassword = req.body.password;
  const role = req.body.role;

  // generate a salt then run callback
  bcrypt.genSalt(10, function(err, salt) {
    if (err) { return next(err); }

    // hash (encrypt) our password using the salt
    bcrypt.hash(plainPassword, salt, null, function(err, password) {
      if (err) { return next(err); }

      if (!email || !password || !role) {
        return res.status(422).send({ error: 'You must provide atleast email, password and role'});
      }
    
      // See if a user with the given email exists
      
      User.findOne({ where: {email: email} }).then(existingUser => {
        if (existingUser != null) {
          return res.status(422).send({ error: 'Email is in use' });
        }
        req.body.password = password;
         // If a user with email does NOT exist, create and save user record
        User.create(req.body).then(function(user){
          // Repond to request indicating the user was created
          res.json({ token: tokenForUser(user), success: "user created" });
        });
      });
      
    });
  });
  
}

exports.update = function(req, res, next) {
  const userId = req.body.id;
  const userUpdate = req.body.user;

  User.findById(userId).then(user => {
    user.update(userUpdate).then((usr) => {
      res.send(usr);
    })
  });
}

exports.user = function(req, res, next) {
  const userId = req.body.id;
  
  User.findById(userId).then(usr => {
      res.send(usr);
  });
}

exports.users = function(req, res, next) {
  User.findAll( ).then(usrs => {
      console.log(usrs);
      res.send(usrs);
  });
}

exports.remove = function(req, res, next) {
  const userId = req.body.id;

  User.findById(userId).then(user => {
    user.destroy();
    res.send("deleted");
  });
}
