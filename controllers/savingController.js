const models = require('../models');
const Customer = models.Customer;
const Saving = models.Saving;
const savingInstallment = require('../services/savingCalculator');

module.exports = {

    index(req, res){
        Customer.findAll({include: [ {
            model: models.Account,
            include: [{
                model: models.Saving, 
                include: [models.Installment]
            }]} ]}).then(customers => {
            res.send(customers);
        });
    },
    create(req, res, next){
        const saving = req.body;
        Saving.create(saving).then(saving => {
            res.send(saving);
        })
    },
    approve(req, res, next){
        res.send('upprove and generate instalment ');
    },
    generate(req, res, next){
        const lId = req.body.savingId
        Saving.findById(lId).then(saving =>{
            const installments = savingInstallment(saving.amount, saving.installmentsNumber,saving.interestRate);
            const Installment = models.Installment;
            const imap = installments.payments.map((instalment, index) => {
                const ins = {
                    installmentId: index+1,
                    capital: instalment.capitalSum,
                    interest: instalment.interest,
                    remain: instalment.sum,
                    interestSum: instalment.sum - instalment.capitalSum,
                    installment: instalment.capital,
                    installmentType: 1,
                    SavingId: saving.id,
                    status: 1
                }
                return ins
            });
            Installment.bulkCreate(imap).then((instalments) => {
                res.send(instalments);
            }) 
        })
    },
    update(req, res, next){
        res.send('NOT IMPLEMENTED: update saving ');
    }

}