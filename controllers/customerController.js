const models = require('../models');
const Customer = models.Customer;

module.exports = {

    index(req, res, next){
        Customer.findAll({include: [ {
            model: models.Account,
            include: [models.Transaction]
            } ]}).then(customers => {
            res.send(customers);
        });
    },

    showCustomer(req, res, next) {
        const cId = req.body.id;
        Customer.findOne({
            where: {id: cId},
            include: [{
                model: models.Account, 
                where: {CustomerId: models.sequelize.literal('Customer.id')},
                include: [{
                    required: false,
                    model: models.Transaction,
                    where: {accountId: models.sequelize.literal('Accounts.id')}
                }]
            }]}).then(customer => {
                res.send(customer);
            });
    },

    create(req,res,next){
        const cNumber = Math.floor(Math.random() * (99999 - 1000) ) + 1000;
        req.body.customer.customerNumber = cNumber;

        Customer.create(req.body.customer).then(customer => {
                    models.Account.create({
                        accountNumber: req.body.accNo,
                        accountTitle: "customer",
                        accountType: "disposal",
                        balance: 0,
                        CustomerId: customer.id
                    })
                }).then(() => {
                            res.send({success: "customer created"});
                        });
    },

    update(req, res, next){
        const customerId = req.body.id;
        const customerUpdate = req.body.customer;

        Customer.findById(customerId).then(customer => {
            customer.update(customerUpdate).then((cust) => {
            res.send(cust);
            })
        });
    },

    remove(req, res, next){
        const customerId = req.body.id;

        Customer.findById(customerId).then(customer => {
            customer.destroy();
            res.send("deleted");
        });
    }
    
}