const models = require('../models');
const Installment = models.Installment;
const Voucher = models.Voucher; 
const Transaction = models.Transaction;

module.exports = {
    
    loan(req, res, next){
        
        Installment.findAll({where: {LoanId: req.body.loanId}}).then(installments => {
            res.send(installments);
        });
    },
    saving(req, res, next){
        
        Installment.findAll({where: {SavingId: req.body.savingId}}).then(installments => {
            res.send(installments);
        });
    },
    create(req, res, next){

        const insId = req.body.id;
        const insUpdate = req.body.installment;

        Installment.findById(insId).then(ins => {
            ins.update(insUpdate).then((ins) => {
                const loanUpdate = {
                    capitalSum: ins.capital,
                    interestSum: ins.interestSum
                };
                const Loan = models.Loan;
                Loan.findById(ins.LoanId).then((loan) =>{
                    loan.update(loanUpdate).then((loan)=>{
                        res.send(ins);
                    })
                })
                
            })
        });
    },
    paySaving(req, res, next){

        const insId = req.body.id;
        const insUpdate = req.body.installment;

        Installment.findById(insId).then(ins => {
            ins.update(insUpdate).then((ins) => {
                const savingUpdate = {
                    capitalSum: ins.capital,
                    interestSum: ins.interestSum
                };
                const Saving = models.Saving;
                Saving.findById(ins.SavingId).then((saving) =>{
                    saving.update(savingUpdate).then((saving)=>{
                        res.send(ins);
                    })
                })
                
            })
        });
    },

    submitVoucher(req, res, next){
        //let account = models.Account;
        const voucher = {
                vId: Math.floor(Math.random() * (99999 - 1000) ) + 1000,
                vType: req.body.vType,
                amount: req.body.amount,
                preparedBy: req.body.preparedBy,
                authorisedBy: req.body.authorizedBy,
                confirmedBy: req.body.confirmedBy,
                AccountId: req.body.accountId,
            };
        Voucher.create(voucher).then((voucher)=>{
            //let account = models.Account;
            const transaction = {
                transactionId: Math.floor(Math.random() * (99999 - 1000) ) + 1000,
                transactionType: req.body.type,
                amount: req.body.amount,
                AccountId: req.body.accId,
                VoucherId: voucher.id
            };
            const accNumber = voucher.AccountId;
            Transaction.create(transaction).then((transaction, accNumber)=>{
                let account = models.Account;
                account.findById(req.body.accId).then(acc => {
                    if(transaction.transactionType == "deposit"){
                        acc.balance += transaction.amount;
                    }
                    else{
                        acc.balance -= transaction.amount;
                    } 
                    acc.save().then(() => {})
                });
                account.findOne( {where: { accountNumber: accNumber}}).then(acc => {
                        if(Voucher.vType === 0){
                            acc.balance += Voucher.amount;
                        }
                        else{
                            acc.balance -= Voucher.amount;
                        } 
                        acc.save().then(() => {})
                    });
                    
                    res.send(transaction);
                });
                //res.send(Voucher);
        });   
        
    },

    //TODO: action or route to create multiple(penality, returned loan and interest) voucher and transaction  
    update(req, res, next){
        res.send('NOT IMPLEMENTED: update instalment ');
    }

}