const Authentication = require('./controllers/authentication');
const passportService = require('./services/passport');
const passport = require('passport');
const customerController = require('./controllers/customerController');
const transactionController = require('./controllers/transactionController');
const loanController = require('./controllers/loanController');
const savingController = require('./controllers/savingController');
const voucherController = require('./controllers/voucherController');
const accountTypeController = require('./controllers/accountTypeController');
const accountController = require('./controllers/accountController');
const InstallmentController = require('./controllers/installmentController');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function(app) {
  app.get('/', requireAuth, function(req, res) {
    res.send({ message: 'Super secret code is ABC123' });
  });
  app.post('/signin', requireSignin, Authentication.signin);
  app.post('/signup', Authentication.signup);
  app.put('/user/update', requireAuth, Authentication.update);
  app.post('/user', requireAuth, Authentication.user);
  app.get('/users', requireAuth, Authentication.users);
  app.delete('/user/remove', requireAuth, Authentication.remove);
  app.get('/customers', requireAuth, customerController.index);
  app.post('/customer', requireAuth, customerController.showCustomer);
  app.post('/customer/create', requireAuth, customerController.create);
  app.put('/customer/update', requireAuth, customerController.update);
  app.delete('/customer/remove', requireAuth, customerController.remove);
  app.post('/transaction', requireAuth, transactionController.create);
  app.get('/loan', requireAuth, loanController.index);
  app.post('/loan/apply', requireAuth, loanController.create);
  app.post('/loan/generate', requireAuth, loanController.generate);
  app.put('/loan/update', requireAuth, loanController.update);
  app.post('/loan/approve', requireAuth, loanController.approve);
  app.get('/savings', requireAuth, savingController.index);
  app.post('/saving/generate', requireAuth, savingController.generate);
  app.post('/saving/create', requireAuth, savingController.create);
  app.post('/installment/loan', requireAuth, InstallmentController.loan);
  app.post('/installment/saving', requireAuth, InstallmentController.saving);
  app.post('/installment/pay', requireAuth, InstallmentController.create);
  app.post('/installment/paysaving', requireAuth, InstallmentController.paySaving);
  app.post('/installment/submit', requireAuth, InstallmentController.submitVoucher);
  app.post('/voucher/create', requireAuth, voucherController.create);
  app.post('/voucher/show', requireAuth, voucherController.show);
  app.post('/voucher/filter', requireAuth, voucherController.filter);
  app.put('/voucher/update', requireAuth, voucherController.update);
  app.get('/vouchers', requireAuth, voucherController.index);
  app.get('/accounts', requireAuth, accountTypeController.index);
  app.get('/account/system', requireAuth, accountController.showSystem);
}
