'use strict';

module.exports = (sequelize, DataTypes) => {
    const Loan = sequelize.define('Loan', {
        amount: DataTypes.DOUBLE,
        installmentsNumber: DataTypes.INTEGER,
        interestRate: DataTypes.DOUBLE,
        capitalSum: DataTypes.DOUBLE,
        interestSum: DataTypes.DOUBLE,
        security: DataTypes.STRING,
        loanType: DataTypes.STRING,
        loanStatus: DataTypes.STRING,
      });
    
    Loan.associate = function(models) {
        models.Loan.hasMany(models.Installment);
      };

    return Loan;
}