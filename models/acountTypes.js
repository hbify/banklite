'use strict';
module.exports = (sequelize, DataTypes) => {
  const AccountTypes = sequelize.define('AccountTypes', {
    category: DataTypes.STRING
  });

/*   AccountTypes.bulkCreate([
    { category: 'Asset' },
    { category: 'Liabilty' },
    { category: 'Capital' },
    { category: 'Revenue' },
    { category: 'Expenditure' }
  ]).then(() => { // Notice: There are no arguments here, as of right now you'll have to...
    return AccountTypes.findAll();
  }).then(accTypes => {
    //console.log(accTypes) // ... in order to get the array of user objects
  })  */

  AccountTypes.associate = function(models) {
    models.AccountTypes.hasMany(models.Account);
  };

  return AccountTypes;
};