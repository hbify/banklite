'use strict';

module.exports = (sequelize, DataTypes) => {
    const Saving = sequelize.define('Saving', {
        amount: DataTypes.DOUBLE,
        installmentsNumber: DataTypes.INTEGER,
        interestRate: DataTypes.DOUBLE,
        capitalSum: DataTypes.DOUBLE,
        interestSum: DataTypes.DOUBLE,
        savingType: DataTypes.STRING,
        savingStatus: DataTypes.STRING
      });

      Saving.associate = function(models) {
        models.Saving.hasMany(models.Installment);
      };

    return Saving;
}