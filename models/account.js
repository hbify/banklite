'use strict';

module.exports = (sequelize, DataTypes) => {
    const Account = sequelize.define('Account', {
        accountNumber: DataTypes.STRING,
        accountTitle: DataTypes.STRING,
        balance: DataTypes.DOUBLE,
        accountType: DataTypes.STRING,
      });

     /* Account.bulkCreate([
        { accountTitle: 'Perk Account', accountNumber: '1001', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Disposable Account', accountNumber: '1002', accountType:  'system', balance:  0, balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Cash - crime prevention', accountNumber: '1003', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'cash - crime adminstration', accountNumber: '1004', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'cash - crime police hospital', accountNumber: '1005', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'cash - crime police college', accountNumber: '1006', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'cash - crime investigation', accountNumber: '1007', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'loan installment collection', accountNumber: '1008', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Other collection', accountNumber: '1009', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Office supplies', accountNumber: '1021', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Office supplies depreciation', accountNumber: '1021-10', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Computer', accountNumber: '1022', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Computer depreciation', accountNumber: '1022-10', accountType:  'system', balance:  0, AccountTypeId: 1 },
        { accountTitle: 'Customers saving', accountNumber: '2001', accountType:  'system', balance:  0, AccountTypeId: 2 },
        { accountTitle: 'Insurance', accountNumber: '2002', accountType:  'system', balance:  0, AccountTypeId: 2 },
        { accountTitle: 'Tax Debt', accountNumber: '2003', accountType:  'system', balance:  0, AccountTypeId: 2 },
        { accountTitle: 'Pension', accountNumber: '2004', accountType:  'system', balance:  0, AccountTypeId: 2 },
        { accountTitle: 'Payments', accountNumber: '2005', accountType:  'system', balance:  0, AccountTypeId: 2 },
        { accountTitle: 'Share', accountNumber: '3001', accountType:  'system', balance:  0, AccountTypeId: 3 },
        { accountTitle: 'Petty cash', accountNumber: '3002', accountType:  'system', balance:  0, AccountTypeId: 3 },
        { accountTitle: 'Undivided return', accountNumber: '3003', accountType:  'system', balance:  0, AccountTypeId: 3 },
        { accountTitle: 'Gifts', accountNumber: '3004', accountType:  'system', balance:  0, AccountTypeId: 3 },
        { accountTitle: 'Income from loan interest', accountNumber: '4001', accountType:  'system', balance:  0, AccountTypeId: 4 },
        { accountTitle: 'Income from bank interest', accountNumber: '4002', accountType:  'system', balance:  0, AccountTypeId: 4 },
        { accountTitle: 'Income from registration', accountNumber: '4003', accountType:  'system', balance:  0, AccountTypeId: 4 },
        { accountTitle: 'Income from commission', accountNumber: '4004', accountType:  'system', balance:  0, AccountTypeId: 4 },
        { accountTitle: 'Income from commission', accountNumber: '4005', accountType:  'system', balance:  0, AccountTypeId: 4 },
        { accountTitle: 'Other Income', accountNumber: '4006', accountType:  'system', balance:  0, AccountTypeId: 4 },
        { accountTitle: 'Payeble customer saving interest', accountNumber: '5001', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Employee Salary', accountNumber: '5002', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Allowance', accountNumber: '5003', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Accounting Expenses', accountNumber: '5004', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'tax', accountNumber: '5005', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Stationary', accountNumber: '5006', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Other expenses', accountNumber: '5007', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Customer activity payments', accountNumber: '5008', accountType:  'system', balance:  0, AccountTypeId: 5 },
        { accountTitle: 'Depreciation', accountNumber: '5009', accountType:  'system', balance:  0, AccountTypeId: 5 },
        
      ]).then(() => { // Notice: There are no arguments here, as of right now you'll have to...
        return Account.findAll();
      }).then(accounts => {
        //console.log(accounts) // ... in order to get the array of user objects
    })  */
    
    Account.associate = function(models) {
        models.Account.hasMany(models.Transaction);
        models.Account.hasMany(models.Loan);
        models.Account.hasMany(models.Saving);
        models.Account.hasMany(models.Voucher);
      };

    return Account;
}