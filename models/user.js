'use strict';

const bcrypt = require('bcrypt-nodejs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        role: DataTypes.STRING,
        firstNames: DataTypes.STRING,
        lastName: DataTypes.STRING,
        title: DataTypes.STRING,
        phoneNumber: DataTypes.STRING,
        address: DataTypes.STRING
      },
      {
        getterMethods: {
          fullName() {
            return this.firstNames + ' ' + this.lastName
          }
        },
        setterMethods: {
          fullName(value) {
            const names = value.split(' ');
      
            this.setDataValue('firstNames', names.slice(0, -1).join(' '));
            this.setDataValue('lastName', names.slice(-1).join(' '));
          },
        }
      });


  User.prototype.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
      if (err) { return callback(err); }
  
      callback(null, isMatch);
    });
  }
  return User;
};
