'use strict';
module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', {
    firstNames: DataTypes.STRING,
    lastName: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    address: DataTypes.STRING,
    customerNumber: DataTypes.STRING
  },
  {
    paranoid: true
  }
);

  Customer.associate = function(models) {
    models.Customer.hasMany(models.Account);
  };

  return Customer;
};