'use strict';
module.exports = (sequelize, DataTypes) => {
  const Voucher = sequelize.define('Voucher', {
    vType: DataTypes.INTEGER,
    amount: DataTypes.STRING,
    ref: DataTypes.STRING,
    vId: DataTypes.STRING,
  });

  Voucher.associate = function(models) {
    models.Voucher.hasMany(models.Transaction);
    models.Voucher.belongsTo(models.User, {foreignKey: 'preparedBy'});
    models.Voucher.belongsTo(models.User, {foreignKey: 'authorisedBy'});
    models.Voucher.belongsTo(models.User, {foreignKey: 'confirmedBy'});
  };
  
  return Voucher;
};