'use strict';

module.exports = (sequelize, DataTypes) => {
    const Transaction = sequelize.define('Transaction', {
        transactionId: DataTypes.STRING,
        transactionType: DataTypes.STRING,
        transactionStatus: DataTypes.STRING,
        amount: DataTypes.DOUBLE,
      });
    
      return Transaction;
}