'use strict';

module.exports = (sequelize, DataTypes) => {
    const Installment = sequelize.define('Installment', {
        installmentId: DataTypes.INTEGER,
        capital: DataTypes.DOUBLE,
        interest: DataTypes.DOUBLE,
        installment: DataTypes.DOUBLE,
        remain: DataTypes.DOUBLE,
        interestSum: DataTypes.DOUBLE,
        penality: DataTypes.DOUBLE,
        status: DataTypes.INTEGER,
        instalmentType: DataTypes.STRING,
        duedate: DataTypes.INTEGER
      });

      Installment.associate = function(models) {
        models.Installment.belongsTo(models.Transaction, {foreignKey: 'tId'});
      };

    return Installment;
}