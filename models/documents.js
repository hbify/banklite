'use strict';
module.exports = (sequelize, DataTypes) => {
  const Document = sequelize.define('Document', {
    name: DataTypes.STRING,
    url: DataTypes.STRING,
  });

  Document.associate = function(models) {
    models.Document.hasMany(models.Voucher);
  };
  Document.associate = function(models) {
    models.Document.hasMany(models.Loan);
  };
  Document.associate = function(models) {
    models.Document.hasMany(models.Saving);
  };

  return Document;
};